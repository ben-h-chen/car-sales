-This solution is built with Visual Studio Community 2017.

-It is an asp.net core mvc with angular 7 integrated. 
Reason being the position applied for is a C# MVC developer but the requirement of the test requires a preferred angular framework. 

-All the packages required should be in the repository downloaded, so you can just run the application.

-Otherwise, rebuild the solution to get all dependencies packages downloaded.

-Compile the angular with CLI command "ng build" on the src folder and the app is good to run.

-Some Angular unit tests have been written in app.component.spec and api.service.spec, do an "ng build" and "ng test" on the src folder and you should see them all passed.

-There is also a backend ms unit test project using moq. 
