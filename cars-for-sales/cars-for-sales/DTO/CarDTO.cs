﻿using System.ComponentModel.DataAnnotations;

namespace DTO
{
    public class CarDTO
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Vehicle type is required.")]
        public string VehicleType { get; set; }
        [Required(ErrorMessage = "Make is required.")]
        public string Make { get; set; }
        [Required(ErrorMessage = "Model is required.")]
        public string Model { get; set; }
        [Required(ErrorMessage = "Engine is required.")]
        public string Engine { get; set; }
        [Required(ErrorMessage = "A mininum number of 1 and a maximimum number of 8 is required for doors.")]
        [Range(1, 8)]
        public int Doors { get; set; }
        [Range(1, 18)]
        [Required(ErrorMessage = "A mininum number of 1 and a maximimum number of 18 is required for wheels.")]
        public int Wheels { get; set; }
        [Required(ErrorMessage = "Body type is required.")]
        public string BodyType { get; set; }
    }
}
