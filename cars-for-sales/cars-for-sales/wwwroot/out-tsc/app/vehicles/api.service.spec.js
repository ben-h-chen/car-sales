var _this = this;
import * as tslib_1 from "tslib";
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApiService } from './api.service';
describe('ApiService', function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    var injector, service, httpMock;
    var _this = this;
    return tslib_1.__generator(this, function (_a) {
        beforeEach(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                TestBed.configureTestingModule({
                    imports: [HttpClientTestingModule],
                    providers: [ApiService]
                }).compileComponents();
                return [2 /*return*/];
            });
        }); });
        injector = getTestBed();
        service = injector.get(ApiService);
        httpMock = injector.get(HttpTestingController);
        it('should be created', function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var service;
            return tslib_1.__generator(this, function (_a) {
                service = TestBed.get(ApiService);
                expect(service).toBeTruthy();
                return [2 /*return*/];
            });
        }); });
        it('should return an Observable<Vehicles[]>', function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var vehicles, req;
            return tslib_1.__generator(this, function (_a) {
                vehicles = ['Car', 'Boat', 'Airplane'];
                service.getVehicles().subscribe(function (vehicles) {
                    expect(vehicles.length).toBe(3);
                    expect(vehicles).toEqual(vehicles);
                });
                req = httpMock.expectOne(service.apiUrl + "/vehicle");
                expect(req.request.method).toBe("GET");
                req.flush(vehicles);
                return [2 /*return*/];
            });
        }); });
        return [2 /*return*/];
    });
}); });
//# sourceMappingURL=api.service.spec.js.map