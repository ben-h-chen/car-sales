import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
        this.apiUrl = 'http://localhost:50552/api/';
    }
    ApiService.prototype.getVehicles = function () {
        return this.http.get(this.apiUrl + 'vehicle')
            .pipe(tap(function (vehicles) { return console.log('fetched Vehicles'); }), catchError(this.handleError('getVehicles', [])));
    };
    ApiService.prototype.addCar = function (car) {
        return this.http.post(this.apiUrl + 'car', car, this.httpOptions).pipe(tap(function (response) { return console.log("added Car w/ id=" + response.Id); }), catchError(this.handleError('addCar')));
    };
    ApiService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            debugger;
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // Let the app keep running by returning an empty result.
            return of(result);
        };
    };
    ApiService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], ApiService);
    return ApiService;
}());
export { ApiService };
//# sourceMappingURL=api.service.js.map