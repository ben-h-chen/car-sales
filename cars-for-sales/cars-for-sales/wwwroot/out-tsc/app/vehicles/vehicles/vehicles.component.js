import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var VehiclesComponent = /** @class */ (function () {
    function VehiclesComponent() {
    }
    VehiclesComponent.prototype.ngOnInit = function () {
    };
    VehiclesComponent = tslib_1.__decorate([
        Component({
            selector: 'app-vehicles',
            templateUrl: './vehicles.component.html',
            styleUrls: ['./vehicles.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], VehiclesComponent);
    return VehiclesComponent;
}());
export { VehiclesComponent };
//# sourceMappingURL=vehicles.component.js.map