import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ApiService } from './vehicles/api.service';
import { FormBuilder, Validators } from '@angular/forms';
var AppComponent = /** @class */ (function () {
    function AppComponent(api, formBuilder) {
        this.api = api;
        this.formBuilder = formBuilder;
        this.vehicles = [];
        this.bodyTypes = ['Hatchback', 'Sedan', 'MPV', 'SUV', 'Crossover', 'Coupe', 'Convertible'];
        this.id = 0;
        this.make = '';
        this.model = '';
        this.engine = '';
        this.doors = 0;
        this.wheels = 0;
    }
    AppComponent.prototype.createForm = function () {
        this.carSalesForm = this.formBuilder.group({
            'vehicleType': ['', Validators.required],
            'bodyType': ['', Validators.required],
            'make': [null, Validators.required],
            'model': [null, Validators.required],
            'engine': [null, Validators.required],
            'doors': [0, [Validators.required, Validators.min(1), Validators.max(8)]],
            'wheels': [0, [Validators.required, Validators.min(1), Validators.max(18)]]
        });
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.api.getVehicles()
            .subscribe(function (response) {
            _this.vehicles = response;
            console.log(_this.vehicles);
        }, function (err) { console.log(err); });
    };
    AppComponent.prototype.saveForm = function (carSalesForm) {
        this.api.addCar(this.carSalesForm.value)
            .subscribe(function (response) {
            if (!response) {
                console.log('record ' + response + ' has been created.');
            }
            else {
                console.log('There is a record creation error.');
            }
        }, function (err) {
            console.log(err);
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ApiService,
            FormBuilder])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map