(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"carSalesForm\" (ngSubmit)=\"saveForm(carSalesForm)\">\r\n    <h2>\r\n        Welcome to Carsales!\r\n    </h2>\r\n    <br />\r\n    <h5>Please choose to create a new vehicle</h5>\r\n\r\n    <div class=\"form-group\">\r\n        <select class=\"form-control\" formControlName=\"vehicleType\">\r\n            <option [ngValue]=\"vehicle\" *ngFor=\"let vehicle of vehicles\">{{ vehicle }}</option>\r\n        </select>\r\n        <div *ngIf=\"this.carSalesForm.controls.vehicleType.invalid && (this.carSalesForm.controls.vehicleType.dirty || this.carSalesForm.controls.vehicleType.touched)\" class=\"alert alert-danger\">\r\n            <div *ngIf=\"this.carSalesForm.controls.vehicleType.errors.required\">\r\n                Vehicle type is required.\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <br />\r\n    <br />\r\n\r\n    <div *ngIf=\"this.carSalesForm.controls.vehicleType.value == 'Car' || this.carSalesForm.controls.vehicleType.value == 'Boat' || this.carSalesForm.controls.vehicleType.value == 'Aircraft'\">\r\n        <div class=\"form-group\">\r\n            <label>Make</label>\r\n            <input type=\"text\" formControlName=\"make\" class=\"form-control\" />\r\n            <div *ngIf=\"this.carSalesForm.controls.make.invalid && (this.carSalesForm.controls.make.dirty || this.carSalesForm.controls.make.touched)\" class=\"alert alert-danger\">\r\n                <div *ngIf=\"this.carSalesForm.controls.make.errors.required\">\r\n                    Make is required.\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <label>Model</label>\r\n            <input type=\"text\" formControlName=\"model\" class=\"form-control\" />\r\n            <div *ngIf=\"this.carSalesForm.controls.model.invalid && (this.carSalesForm.controls.model.dirty || this.carSalesForm.controls.model.touched)\" class=\"alert alert-danger\">\r\n                <div *ngIf=\"this.carSalesForm.controls.model.errors.required\">\r\n                    Model is required.\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <label>Engine</label>\r\n            <input type=\"text\" formControlName=\"engine\" class=\"form-control\" />\r\n            <div *ngIf=\"this.carSalesForm.controls.engine.invalid && (this.carSalesForm.controls.engine.dirty || this.carSalesForm.controls.engine.touched)\" class=\"alert alert-danger\">\r\n                <div *ngIf=\"this.carSalesForm.controls.engine.errors.required\">\r\n                    Engine is required.\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"this.carSalesForm.controls.vehicleType.value == 'Car'\">\r\n        <div class=\"form-group\">\r\n            <label>Doors</label>\r\n            <input type=\"number\" formControlName=\"doors\" class=\"form-control\" />\r\n            <div *ngIf=\"this.carSalesForm.controls.doors.invalid && (this.carSalesForm.controls.doors.dirty || this.carSalesForm.controls.doors.touched)\" class=\"alert alert-danger\">\r\n                <div *ngIf=\"this.carSalesForm.controls.doors.errors.required || this.carSalesForm.controls.doors.errors.min || this.carSalesForm.controls.doors.errors.max\">\r\n                    A mininum number of 1 and a maximimum number of 8 is required for doors.\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <label>Wheels</label>\r\n            <input type=\"number\" formControlName=\"wheels\" class=\"form-control\" />\r\n            <div *ngIf=\"this.carSalesForm.controls.wheels.invalid && (this.carSalesForm.controls.wheels.dirty || this.carSalesForm.controls.wheels.touched)\" class=\"alert alert-danger\">\r\n                <div *ngIf=\"this.carSalesForm.controls.wheels.errors.required || this.carSalesForm.controls.wheels.errors.min || this.carSalesForm.controls.wheels.errors.max\">\r\n                    A mininum number of 1 and a maximimum number of 18 is required for wheels.\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n            <label>Body Type</label>\r\n            <select class=\"form-control\" formControlName=\"bodyType\">\r\n                <option [ngValue]=\"bodyType\" *ngFor=\"let bodyType of bodyTypes\">{{ bodyType }}</option>\r\n            </select>\r\n            <div *ngIf=\"this.carSalesForm.controls.bodyType.invalid && (this.carSalesForm.controls.bodyType.dirty || this.carSalesForm.controls.bodyType.touched)\" class=\"alert alert-danger\">\r\n                <div *ngIf=\"this.carSalesForm.controls.bodyType.errors.required\">\r\n                    Body type is required.\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"carSalesForm.controls.vehicleType.value == 'Boat'\">\r\n        <!-- To be implemented -->\r\n    </div>\r\n\r\n    <br />\r\n    <br />\r\n    <div *ngIf=\"carSalesForm.controls.vehicleType.value == 'Car' || carSalesForm.controls.vehicleType.value == 'Boat'\">\r\n        <button type=\"submit\" class=\"btn btn-primary\">Create</button>\r\n    </div>\r\n</form>\r\n<!--<router-outlet></router-outlet>-->\r\n<!--<pre>\r\n    {{carSalesForm.value | json}}\r\n</pre>-->\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvYXBwLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _vehicles_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vehicles/api.service */ "./src/app/vehicles/api.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(api, formBuilder) {
        this.api = api;
        this.formBuilder = formBuilder;
        this.vehicles = [];
        this.bodyTypes = ['Hatchback', 'Sedan', 'MPV', 'SUV', 'Crossover', 'Coupe', 'Convertible'];
        this.id = 0;
        this.make = '';
        this.model = '';
        this.engine = '';
        this.doors = 0;
        this.wheels = 0;
    }
    AppComponent.prototype.createForm = function () {
        this.carSalesForm = this.formBuilder.group({
            'vehicleType': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'bodyType': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'make': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'model': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'engine': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'doors': [0, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(8)]],
            'wheels': [0, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(18)]]
        });
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.api.getVehicles()
            .subscribe(function (response) {
            _this.vehicles = response;
            console.log(_this.vehicles);
        }, function (err) { console.log(err); });
    };
    AppComponent.prototype.saveForm = function (carSalesForm) {
        this.api.addCar(this.carSalesForm.value)
            .subscribe(function (response) {
            if (!response) {
                console.log('record ' + response + ' has been created.');
            }
            else {
                console.log('There is a record creation error.');
            }
        }, function (err) {
            console.log(err);
        });
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_vehicles_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]
            ],
            providers: [{ provide: _angular_common__WEBPACK_IMPORTED_MODULE_7__["APP_BASE_HREF"], useValue: '/' }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/vehicles/api.service.ts":
/*!*****************************************!*\
  !*** ./src/app/vehicles/api.service.ts ***!
  \*****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Content-Type': 'application/json' })
        };
        this.apiUrl = 'http://localhost:50552/api/';
    }
    ApiService.prototype.getVehicles = function () {
        return this.http.get(this.apiUrl + 'vehicle')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (vehicles) { return console.log('fetched Vehicles'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError('getVehicles', [])));
    };
    ApiService.prototype.addCar = function (car) {
        return this.http.post(this.apiUrl + 'car', car, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (response) { return console.log("added Car w/ id=" + response.Id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError('addCar')));
    };
    ApiService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            debugger;
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // Let the app keep running by returning an empty result.
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(result);
        };
    };
    ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\projects\car-sales\cars-for-sales\cars-for-sales\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map