﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Controllers
{
    public class VehicleController : Controller
    {
        private const string CompanyName = "Carsales.com Ltd";
        
        public IActionResult Car()
        {
            return View();
        }

        public IActionResult Boat()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
