﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Behaviours.Interfaces;

namespace Controllers.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        readonly IVehicleBehaviors _vehicleBehaviors;

        public VehicleController(IVehicleBehaviors vehicleBehaviors)
        {
            _vehicleBehaviors = vehicleBehaviors;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            try
            {
                var vehicleTypes = _vehicleBehaviors.Get();
                return Ok(vehicleTypes);
            }
            catch (Exception ex)
            {
                var error = ex.InnerException;
                // log the error.
                return StatusCode(500);
            }
        }
    }
}
