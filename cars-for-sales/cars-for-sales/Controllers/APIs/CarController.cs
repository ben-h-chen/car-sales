﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Models;
using DTO;
using AutoMapper;
using Behaviours.Interfaces;

namespace Controllers.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : ControllerBase
    {
        readonly ICarBehaviors _carBehaviors;

        public CarController(ICarBehaviors carBehaviors) {
            _carBehaviors = carBehaviors;
        }

        [HttpPost]
        public ActionResult<long> Post([FromBody] CarDTO carDTO)
        {
            var _carDTO = carDTO;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var car = Mapper.Map<CarDTO, Car>(_carDTO);
                var exist = CarData.cars.Where(c => c.Make == _carDTO.Make && c.Model == _carDTO.Model && c.Engine == _carDTO.Engine && c.BodyType == _carDTO.BodyType).ToList();
                if (exist?.Count > 0) {
                    return BadRequest("This car has already been added.");
                }
                var carId = _carBehaviors.Add(car);
                return Ok(carId);
            }
            catch (Exception ex)
            {
                var error = ex.InnerException;
                // log the error.
                return StatusCode(500);
            }
            
        }
    }
}
