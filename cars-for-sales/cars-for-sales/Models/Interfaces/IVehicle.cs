﻿namespace Interfaces
{
    interface IVehicle
    {
        long Id { get; set; }
        string VehicleType { get; set; }
        string Make { get; set; }
        string Model { get; set; }
        string Engine { get; set; }
    }
}
