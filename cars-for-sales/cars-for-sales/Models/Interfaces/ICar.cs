﻿namespace Interfaces
{
    interface ICar: IVehicle
    {
        int Doors { get; set; }
        int Wheels { get; set; }
        string BodyType { get; set; }
    }
}
