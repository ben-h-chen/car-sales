﻿using Interfaces;

namespace Models
{
    public class Car : Vehicle, ICar 
    {
        public int Doors { get; set; }
        public int Wheels { get; set; }
        public string BodyType { get; set; }
    }
}
