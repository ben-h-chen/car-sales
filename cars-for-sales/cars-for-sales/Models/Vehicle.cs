﻿using Interfaces;

namespace Models
{
    public class Vehicle: IVehicle
    {
        public long Id { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
    }
}
