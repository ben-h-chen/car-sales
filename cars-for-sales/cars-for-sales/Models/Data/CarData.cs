﻿using System.Collections.Generic;

namespace Models
{
    public class CarData
    {
        public static List<Car> cars = new List<Car>(){
                new Car {
                     Id = 1,
                     VehicleType = "Car",
                     Make = "Audi",
                     Model = "AS",
                     Engine = "3.0 TFSI",
                     Doors = 4,
                     Wheels = 4,
                     BodyType = "Sedan"
                },
                new Car {
                     Id = 1,
                     VehicleType = "Car",
                     Make = "Alfa Romeo",
                     Model = "S6",
                     Engine = "1.6 Scale",
                     Doors = 4,
                     Wheels = 4,
                     BodyType = "Sedan"
                },
                new Car {
                     Id = 1,
                     VehicleType = "Car",
                     Make = "Fiat",
                     Model = "FQ7",
                     Engine = "427 SOHC",
                     Doors = 4,
                     Wheels = 4,
                     BodyType = "Sedan"
                },
                new Car {
                     Id = 1,
                     VehicleType = "Car",
                     Make = "BMW",
                     Model = "TT",
                     Engine = "426 Top",
                     Doors = 4,
                     Wheels = 4,
                     BodyType = "Sedan"
                },
            };
    }
}
