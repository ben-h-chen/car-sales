﻿using Behaviours.Interfaces;

namespace Behaviours
{
    public class VehicleBehaviors : IVehicleBehaviors
    {
        public string[] Get() {
            return new string[] { "Car", "Boat", "Airplane" };
        }
    }
}
