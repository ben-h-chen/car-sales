﻿using Interfaces;
using Models;

namespace Behaviours.Interfaces
{
    public interface ICarBehaviors
    {
        long Add(Car car);
    }
}
