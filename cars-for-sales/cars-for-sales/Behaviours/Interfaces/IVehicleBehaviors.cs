﻿namespace Behaviours.Interfaces
{
    public interface IVehicleBehaviors
    {
        string[] Get();
    }
}
