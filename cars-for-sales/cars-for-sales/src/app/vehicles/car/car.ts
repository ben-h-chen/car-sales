import { Vehicle } from '../vehicle';

export interface Car extends Vehicle {
    Doors: number;
    Wheels: number;
    BodyType: string;
}
