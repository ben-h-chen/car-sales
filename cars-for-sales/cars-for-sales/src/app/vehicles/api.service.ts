import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Car } from './car/car';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
    readonly httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    readonly apiUrl = 'http://localhost:50552/api/';

    constructor(private http: HttpClient) { }

    getVehicles(): Observable<string[]> {
        return this.http.get<string[]>(this.apiUrl + 'vehicle')
        .pipe(
        tap(vehicles => console.log('fetched Vehicles')),
        catchError(this.handleError('getVehicles', []))
        );
    }

    addCar(car: Car): Observable<Car> {
        return this.http.post<Car>(this.apiUrl + 'car', car, this.httpOptions).pipe(
            tap((response: Car) => console.log(`added Car w/ id=${response.Id}`)),
            catchError(this.handleError<Car>('addCar'))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            debugger;
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
