export interface Vehicle {
    Id: number;
    VehicleType: string;
    Make: string;
    Model: string;
    Engine: string;
}
