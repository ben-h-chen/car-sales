import { TestBed, getTestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApiService } from './api.service';

describe('ApiService', async() => {
    let injector: TestBed;
    let service: ApiService;
    let httpMock: HttpTestingController;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [ApiService]
        }).compileComponents();
    });

    injector = getTestBed();
    service = injector.get(ApiService);
    httpMock = injector.get(HttpTestingController);

    it('should be created', async() => {
        const service: ApiService = TestBed.get(ApiService);
        expect(service).toBeTruthy();
    });

    it('should return an Observable<Vehicles[]>', async() => {
        const vehicles = ['Car', 'Boat', 'Airplane'];

        service.getVehicles().subscribe(vehicles => {
            expect(vehicles.length).toBe(3);
            expect(vehicles).toEqual(vehicles);
        });

        const req = httpMock.expectOne(`${service.apiUrl}/vehicle`);
        expect(req.request.method).toBe("GET");
        req.flush(vehicles);
    });
});

