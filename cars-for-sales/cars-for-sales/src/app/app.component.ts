import { Component, OnInit } from '@angular/core';
import { ApiService } from './vehicles/api.service';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

    carSalesForm: FormGroup;

    constructor(private api: ApiService,
                private formBuilder: FormBuilder) {
    }

    
    vehicles: string[] = [];

    bodyTypes: string[] = ['Hatchback', 'Sedan', 'MPV', 'SUV', 'Crossover', 'Coupe', 'Convertible'];
  
    id: number = 0;
    make: string = '';
    model: string = '';
    engine: string = '';
    doors: number = 0;
    wheels: number = 0;
 
    createForm() {
        this.carSalesForm = this.formBuilder.group({
            'vehicleType': ['', Validators.required],
            'bodyType' : ['', Validators.required],
            'make': [null, Validators.required],
            'model': [null, Validators.required],
            'engine': [null, Validators.required],
            'doors': [0, [Validators.required, Validators.min(1), Validators.max(8)]],
            'wheels': [0, [Validators.required, Validators.min(1), Validators.max(18)]]
        });
    }

    ngOnInit() {
        this.createForm();
        this.api.getVehicles()
            .subscribe(response => {
                this.vehicles = response;
                console.log(this.vehicles);
                },
            err => { console.log(err); }
        );
    }

    saveForm(carSalesForm: FormGroup) {
        this.api.addCar(this.carSalesForm.value)
            .subscribe((response: { [x: string]: any; }) => {
                if (!response) {
                    console.log('record ' + response + ' has been created.');
                }
                else {
                    console.log('There is a record creation error.');
                }
            }, (err) => {
                    console.log(err);
            });
    }

}
