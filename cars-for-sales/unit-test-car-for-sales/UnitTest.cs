using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using Moq;
using Behaviours.Interfaces;
using Controllers.APIs;
using DTO;
using Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace unit_test_car_for_sales
{
    [TestClass]
    public class UnitTest
    {
        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            Mapper.Initialize((config) =>
            {
                config.CreateMap<Car, CarDTO>().ReverseMap();
            });
        }

        [TestMethod]
        public void TestGetVehicles()
        {
            // Arrange
            var mock = new Mock<IVehicleBehaviors>();
            mock.Setup(v => v.Get()).Returns(new string[] { "Car", "Boat", "Airplane" });

            // Action
            var vehicleController = new VehicleController(mock.Object);
            var actionResult = vehicleController.Get().Result;

            // Assert
            var okObjectResult = actionResult as Microsoft.AspNetCore.Mvc.OkObjectResult;
            Assert.IsNotNull(okObjectResult);
            var actualResult = okObjectResult.Value as string[];
            Assert.IsNotNull(actualResult);
            CollectionAssert.AreEqual(new string[] { "Car", "Boat", "Airplane" }, actualResult, StructuralComparisons.StructuralComparer);
        }

        [TestMethod]
        public void TestAddCar()
        {
            // Arrange
            var mock = new Mock<ICarBehaviors>();
            long carId = 4;
            mock.Setup(c => c.Add(It.IsAny<Car>())).Returns(carId);
            var carDTO = new CarDTO()
            {
                Id = 0,
                VehicleType = "Car",
                Make = "Nissan",
                Model = "Fuga",
                Engine = "8cyl 4.5L",
                Doors = 4,
                Wheels = 5,
                BodyType = "Sedan"
            };

            // Action
            var carController = new CarController(mock.Object);
            var actionResult = carController.Post(carDTO).Result;

            // Assert
            var okObjectResult = actionResult as OkObjectResult;
            Assert.IsNotNull(okObjectResult);
            var actualResult = okObjectResult.Value;
            Assert.IsNotNull(actualResult);
            Assert.AreEqual(carId, actualResult);
        }

        [TestMethod]
        public void TestDuplicateCar()
        {
            // Arrange
            var mock = new Mock<ICarBehaviors>();
              var carDTO = new CarDTO()
            {
                Id = 1,
                VehicleType = "Car",
                Make = "Audi",
                Model = "AS",
                Engine = "3.0 TFSI",
                Doors = 4,
                Wheels = 4,
                BodyType = "Sedan"
            };

            // Action
            var carController = new CarController(mock.Object);
            var actionResult = carController.Post(carDTO).Result;

            // Assert
            var badRequestObjectResult = actionResult as BadRequestObjectResult;
            Assert.IsNotNull(badRequestObjectResult);
            Assert.IsInstanceOfType(badRequestObjectResult, typeof(BadRequestObjectResult));
            Assert.AreEqual("This car has already been added.", badRequestObjectResult.Value);
        }
    }
}
